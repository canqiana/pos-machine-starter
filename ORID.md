Objective: 

1. Learn the meaning of tasking and understand that get the demand, it is necessary to clarify the demand first, analyze it, then divide the task, draw the Context Map, and code at last.
2. Learn what is Context Map composed of and how to draw a Context Map.
3. Learn the rules of naming and practice it with teammates, "Seeing the name and knowing the meaning". 
4. Learn common git operations and how to write git commit notes. 
5. After drawing Context Map, Write code to implement every step in the map.

Reflective:  Fruitful and Challenging.

Interpretive:  I learn a lot of new knowledge in this day's study，such as Tasking and Context Map. I encountered some difficulties in the process of drawing Context Map, such as inaccurate task division, unclear data structure, etc. 

Decisional:  Context map are very helpful for me to break down complex problems into simple ones and let me to solve complex problems more efficiently. I will practice drawing Context Map more in the future. I will also actively seek the help of teachers and classmates when I have problem.