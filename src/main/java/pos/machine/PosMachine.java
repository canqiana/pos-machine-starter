package pos.machine;

import java.util.*;
import static pos.machine.ItemsLoader.loadAllItems;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public static List<ReceiptItem> decodeToItems(List<String> barcodes) {
        List<Item> items = loadAllItems();
        List<ReceiptItem> receiptItems = new ArrayList<ReceiptItem>();
        HashMap<String, Integer> map = new HashMap<>();
        for (String barcode : barcodes) {
            map.put(barcode, map.getOrDefault(barcode, 0) + 1);
        }

        for (Item item : items) {
            String barcode = item.getBarcode();
            if (map.containsKey(barcode)) {
                receiptItems.add(new ReceiptItem(item.getName(), map.get(barcode), item.getPrice(), 0));
            }
        }
        return receiptItems;
    }

    private static int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    private static List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
        }
        return receiptItems;
    }

    public static Receipt calculateCost(List<ReceiptItem> receiptItems) {
        return new Receipt(calculateItemsCost(receiptItems), calculateTotalPrice(receiptItems));
    }

    public String generateItemReceipt(ReceiptItem receiptItem) {
        StringJoiner itemReceipt = new StringJoiner(", ");
        itemReceipt.add("Name: " + receiptItem.getName());
        itemReceipt.add("Quantity: " + receiptItem.getQuantity());
        itemReceipt.add("Unit price: " + receiptItem.getUnitPrice() + " (yuan)");
        itemReceipt.add("Subtotal: " + receiptItem.getSubTotal() + " (yuan)");
        return itemReceipt.toString();
    }

    public String generateItemsReceipt(Receipt receipt) {
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        StringBuilder itemsReceipt = new StringBuilder();
        for (ReceiptItem receiptItem : receiptItems) {
            itemsReceipt.append(generateItemReceipt(receiptItem));
            itemsReceipt.append("\n");
        }
        return itemsReceipt.toString();
    }

    public String generateReceipt(String itemsReceipt, int totalPrice) {
        String receipt = "***<store earning no money>Receipt***\n" +
                itemsReceipt +
                "----------------------\n" +
                "Total: " + totalPrice + " (yuan)\n" +
                "**********************";
        return receipt;
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt, receipt.getTotalPrice());
    }
}
